import Area from './Area';

export default interface Employee {
  id?: number;
  email?: string;
  createdAt?: Date;
  updatedAt?: Date;
  nameOne: string;
  otherNames: string;
  surnameOne: string;
  surnameTwo: string;
  numID: string;
  country: string;
  area: Area;
  typeId: string;
  admission?: Date;
  state?: boolean;
}
