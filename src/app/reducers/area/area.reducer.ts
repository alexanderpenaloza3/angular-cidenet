import Area from '../../interfaces/Area';
import * as fromActions from './area.actions';

export interface AreaState {
  areas: Area[];
}

const initState: AreaState = {
  areas: [],
};

export const areaReducer = (
  state = initState,
  actions: fromActions.actions
): AreaState => {
  switch (actions.type) {
    case fromActions.ADD_AREA:
      return {
        ...state,
        areas: [...state.areas, actions.area],
      };

    case fromActions.LOAD_AREAS:
      return {
        ...state,
        areas: [...actions.areas],
      };

    default:
      return state;
  }
};
