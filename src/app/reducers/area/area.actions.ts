import { Action } from '@ngrx/store';
import Area from '../../interfaces/Area';

// Tipos
export const ADD_AREA = '[AREA] Agregar Area';
export const LOAD_AREAS = '[AREA] Cargar Areas';

// Acciones
export class AddAreaAction implements Action {
  readonly type = ADD_AREA;
  constructor(public area: Area) {}
}

export class LoadAreasAction implements Action {
  readonly type = LOAD_AREAS;
  constructor(public areas: Area[]) {}
}

export type actions = AddAreaAction | LoadAreasAction;
