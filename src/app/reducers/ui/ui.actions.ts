import { Action } from '@ngrx/store';

// Tipos
export const START_LOADING = '[UI] Iniciar carga';
export const FINISH_LOADING = '[UI] Finalizar carga';

// Acciones
export class StartLoadingAction implements Action {
  readonly type = START_LOADING;
  constructor() {}
}

export class FinishLoadingAction implements Action {
  readonly type = FINISH_LOADING;
  constructor() {}
}

export type actions = StartLoadingAction | FinishLoadingAction;
