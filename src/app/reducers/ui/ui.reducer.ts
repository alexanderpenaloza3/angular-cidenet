import * as fromActions from '../ui/ui.actions';

export interface UiState {
  loading: boolean;
}

const initState: UiState = {
  loading: false,
};

export const uiReducer = (
  state = initState,
  actions: fromActions.actions
): UiState => {
  switch (actions.type) {
    case fromActions.START_LOADING:
      return {
        ...state,
        loading: true,
      };

    case fromActions.FINISH_LOADING:
      return {
        ...state,
        loading: false,
      };

    default:
      return {
        ...state,
      };
  }
};
