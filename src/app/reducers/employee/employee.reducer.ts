import Employee from '../../interfaces/Employee';
import * as fromActions from '../employee/employee.actions';

export interface EmployeeState {
  employees: Employee[];
  active: Employee;
  totalPages: number;
  totalEmployees: number;
}

const initState: EmployeeState = {
  employees: [],
  active: null,
  totalPages: 1,
  totalEmployees: 0,
};

export const employeeReducer = (
  state = initState,
  actions: fromActions.actions
): EmployeeState => {
  switch (actions.type) {
    case fromActions.ADD_EMPLOYEE:
      return {
        ...state,
        employees: [actions.employee, ...state.employees],
      };

    case fromActions.LOAD_EMPLOYEES:
      return {
        ...state,
        employees: [...actions.employees],
      };

    case fromActions.SET_ACTIVE:
      return {
        ...state,
        active: { ...actions.employee },
      };

    case fromActions.UNSET_ACTIVE:
      return {
        ...state,
        active: null,
      };

    case fromActions.DELETE_EMPLOYEE:
      return {
        ...state,
        employees: state.employees.filter(
          (employee) => employee.id !== actions.id
        ),
      };

    case fromActions.SET_TOTAL_EMPLOYEES:
      return {
        ...state,
        totalEmployees: actions.totalEmployees,
      };

    case fromActions.SET_TOTAL_PAGES:
      return {
        ...state,
        totalPages: actions.totaPages,
      };

    case fromActions.UPDATE_EMPLOYEE:
      return {
        ...state,
        employees: state.employees.map((employee) =>
          employee.id === actions.id ? { ...actions.employee } : employee
        ),
      };

    default:
      return {
        ...state,
      };
  }
};
