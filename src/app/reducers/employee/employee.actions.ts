import { Action } from '@ngrx/store';
import Employee from '../../interfaces/Employee';

// Tipos
export const ADD_EMPLOYEE = '[EMPLOYEE] Agregar Empleado';
export const LOAD_EMPLOYEES = '[EMPLOYEE] Listar Empleados';
export const SET_ACTIVE = '[EMPLOYEE] Activar Empleado';
export const UNSET_ACTIVE = '[EMPLOYEE] Desactivar Empleado';
export const DELETE_EMPLOYEE = '[EMPLOYEE] Eliminar Empleado';
export const SET_TOTAL_PAGES = '[EMPLOYEE] Actualizar total de páginas';
export const SET_TOTAL_EMPLOYEES = '[EMPLOYEE] Actualizar total de empleados';
export const UPDATE_EMPLOYEE = '[EMPLOYEE] Actualizar empleado';

// Acciones
export class AddEmployeeAction implements Action {
  readonly type = ADD_EMPLOYEE;
  constructor(public employee: Employee) {}
}

export class LoadEmployeesAction implements Action {
  readonly type = LOAD_EMPLOYEES;
  constructor(public employees: Employee[]) {}
}

export class SetEmployeeAction implements Action {
  readonly type = SET_ACTIVE;
  constructor(public employee: Employee) {}
}

export class UnsetEmployeeAction implements Action {
  readonly type = UNSET_ACTIVE;
  constructor() {}
}

export class DeleteEmployeeAction implements Action {
  readonly type = DELETE_EMPLOYEE;
  constructor(public id: number) {}
}

export class SetTotalPagesAction implements Action {
  readonly type = SET_TOTAL_PAGES;
  constructor(public totaPages: number) {}
}

export class SetTotalEmployeesAction implements Action {
  readonly type = SET_TOTAL_EMPLOYEES;
  constructor(public totalEmployees: number) {}
}

export class UpdateEmployeeAction implements Action {
  readonly type = UPDATE_EMPLOYEE;
  constructor(public id: number, public employee: Employee) {}
}

export type actions =
  | SetTotalPagesAction
  | SetTotalEmployeesAction
  | SetEmployeeAction
  | UnsetEmployeeAction
  | AddEmployeeAction
  | LoadEmployeesAction
  | DeleteEmployeeAction
  | UpdateEmployeeAction;
