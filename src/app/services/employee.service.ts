import { Injectable } from '@angular/core';
import { sendRequest } from '../helpers/fetch';
import Employee from '../interfaces/Employee';

@Injectable({
  providedIn: 'root',
})
export class EmployeeService {
  constructor() {}

  createEmployee = async (employee: Employee) => {
    try {
      const req = await sendRequest('api/employees', 'POST', employee);
      return req.status < 300
        ? {
            ok: true,
            body: await req.json(),
          }
        : {
            ok: false,
            msg: 'Empleado duplicado',
          };
    } catch (error) {
      console.error(error);
    }
  };

  loadEmployees = async (page: number = 1) => {
    try {
      const req = await sendRequest(`api/employees?page=${page}`);
      const body = await req.json();
      return body;
    } catch (error) {
      console.error(error);
    }
  };

  deleteEmployee = async (id: number) => {
    try {
      await sendRequest(`api/employees/${id}`, 'DELETE', null);
    } catch (error) {
      console.error(error);
    }
  };

  updateEmployee = async (id: number, employee: Employee) => {
    try {
      const req = await sendRequest(`api/employees/${id}`, 'PUT', employee);
      return req.status < 300
        ? {
            ok: true,
            body: await req.json(),
          }
        : {
            ok: false,
            msg: 'Empleado duplicado',
          };
    } catch (error) {
      console.error(error);
    }
  };
}
