import { Injectable } from '@angular/core';
import { sendRequest } from '../helpers/fetch';
import Area from '../interfaces/Area';

@Injectable({
  providedIn: 'root',
})
export class AreaService {
  constructor() {}

  loadAreas = async () => {
    try {
      const req = await sendRequest('api/areas');
      const body = await req.json();
      return body;
    } catch (error) {
      console.error(error);
    }
  };

  createArea = async (area: Area) => {
    try {
      const req = await sendRequest('api/areas', 'POST', area);
      const body = req.json();
      return body;
    } catch (error) {
      console.error(error);
    }
  };
}
