import { Pipe, PipeTransform } from '@angular/core';
import Employee from '../interfaces/Employee';

@Pipe({
  name: 'filter',
})
export class FilterPipe implements PipeTransform {
  transform(value: Employee[], arg: string): Employee[] {
    arg = arg.toLowerCase();
    const data: Employee[] = [];
    for (const employee of value) {
      if (
        employee.nameOne.toLowerCase().indexOf(arg) > -1 ||
        employee.otherNames?.toLowerCase().indexOf(arg) > -1 ||
        employee.surnameOne.toLowerCase().indexOf(arg) > -1 ||
        employee.surnameTwo.toLowerCase().indexOf(arg) > -1 ||
        employee.email.toLowerCase().indexOf(arg) > -1 ||
        employee.numID.toLowerCase().indexOf(arg) > -1
      ) {
        data.push(employee);
      }
    }
    return data;
  }
}
