import { Pipe, PipeTransform } from '@angular/core';
import { showMessage } from '../helpers/alerts';
import Employee from '../interfaces/Employee';

@Pipe({
  name: 'filterByCountry',
})
export class FilterByCountryPipe implements PipeTransform {
  transform(value: Employee[], arg: string): Employee[] {
    const data: Employee[] = [];
    for (const employee of value) {
      if (employee.country === arg || employee.typeId === arg) {
        data.push(employee);
      }
    }
    if (!data.length && arg.length) {
      showMessage('warning', 'No se encontrarón resultados');
    }
    return data.length ? data : value;
  }
}
