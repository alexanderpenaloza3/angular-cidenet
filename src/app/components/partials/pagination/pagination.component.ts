import { Component, OnDestroy, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
import { AppState } from '../../../app.reducers';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styles: [],
})
export class PaginationComponent implements OnInit, OnDestroy {
  // Variables
  employeeSubscription: Subscription = new Subscription();
  pages: number[];

  constructor(private store: Store<AppState>) {
    this.employeeSubscription = this.store.select('employee').subscribe(
      ({ totalPages }) =>
        (this.pages = Array(totalPages)
          .fill(1)
          .map((n, i) => i + 1))
    );
  }

  ngOnInit(): void {}

  // Cancelar subscripciones
  ngOnDestroy(): void {
    this.employeeSubscription.unsubscribe();
  }
}
