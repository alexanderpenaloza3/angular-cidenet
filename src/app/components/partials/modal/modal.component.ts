import { Component, OnDestroy, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import {
  SetEmployeeAction,
  UnsetEmployeeAction,
  UpdateEmployeeAction,
} from 'src/app/reducers/employee/employee.actions';
import { AppState } from '../../../app.reducers';
import Area from '../../../interfaces/Area';
import Employee from 'src/app/interfaces/Employee';
import { EmployeeService } from 'src/app/services/employee.service';
import { showMessage } from 'src/app/helpers/alerts';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styles: [],
})
export class ModalComponent implements OnInit, OnDestroy {
  // Variables
  areaSubscription: Subscription = new Subscription();
  employeeSubscription: Subscription = new Subscription();
  areas: Area[];
  active: Employee = null;

  // Estructura formulario reactivo
  createFormGroup(
    nameOne,
    otherNames,
    surnameOne,
    surnameTwo,
    numID,
    country,
    area,
    typeId
  ) {
    const regex1 = new RegExp(/^[A-Z]+$/i);
    const regex2 = new RegExp(/^[A-Z0-9-]+$/i);
    const regex3 = new RegExp(/^[A-Z\s]+$/i);
    return new FormGroup({
      nameOne: new FormControl(nameOne, [
        Validators.required,
        Validators.maxLength(20),
        Validators.pattern(regex1),
      ]),
      otherNames: new FormControl(otherNames, [
        Validators.maxLength(50),
        Validators.pattern(regex1),
      ]),
      surnameOne: new FormControl(surnameOne, [
        Validators.required,
        Validators.maxLength(20),
        Validators.pattern(regex3),
      ]),
      surnameTwo: new FormControl(surnameTwo, [
        Validators.required,
        Validators.maxLength(20),
        Validators.pattern(regex3),
      ]),
      numID: new FormControl(numID, [
        Validators.required,
        Validators.pattern(regex2),
      ]),
      country: new FormControl(country, Validators.required),
      area: new FormControl(area, Validators.required),
      typeId: new FormControl(typeId, Validators.required),
    });
  }
  updateForm: FormGroup;

  constructor(
    private store: Store<AppState>,
    private employeeService: EmployeeService
  ) {
    this.areaSubscription = this.store
      .select('area')
      .subscribe(({ areas }) => (this.areas = areas));
    this.employeeSubscription = this.store
      .select('employee')
      .subscribe(({ active }) => {
        this.active = active;
        if (active) {
          this.updateForm = this.createFormGroup(
            active.nameOne,
            active.otherNames,
            active.surnameOne,
            active.surnameTwo,
            active.numID,
            active.country,
            active.area.id,
            active.typeId
          );
        }
      });
  }

  ngOnInit(): void {}

  // Cancelar subscripciones
  ngOnDestroy(): void {
    this.areaSubscription.unsubscribe();
  }

  async onSubmit() {
    if (this.updateForm.valid) {
      let updateEmployee: Employee = {
        ...this.updateForm.value,
        area: {
          id: parseInt(this.updateForm.value.area),
        },
        otherNames: !this.updateForm.value.otherNames
          ? null
          : this.updateForm.value.otherNames,
      };

      const res = await this.employeeService.updateEmployee(
        this.active.id,
        updateEmployee
      );

      if (res.ok) {
        updateEmployee = res.body;
        this.store.dispatch(
          new UpdateEmployeeAction(this.active.id, updateEmployee)
        );
        this.store.dispatch(new SetEmployeeAction(updateEmployee));
        showMessage('success', 'Actualizado');
      } else {
        showMessage('error', res.msg);
      }
    }
  }

  deactivateEmployee() {
    this.store.dispatch(new UnsetEmployeeAction());
  }
}
