import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styles: [],
})
export class NavbarComponent implements OnInit {
  currentPage: number;

  constructor(private route: ActivatedRoute) {
    this.route.params.subscribe((params) => (params.id = this.currentPage));
  }

  ngOnInit(): void {}
}
