import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import Employee from '../../interfaces/Employee';
import Area from '../../interfaces/Area';
import { showMessage, showQuestion } from '../../helpers/alerts';
import { AppState } from '../../app.reducers';
import { AreaService } from '../../services/area.service';
import { EmployeeService } from '../../services/employee.service';
import { AddAreaAction } from '../../reducers/area/area.actions';
import {
  FinishLoadingAction,
  StartLoadingAction,
} from '../../reducers/ui/ui.actions';

@Component({
  selector: 'app-add-employee',
  templateUrl: './add-employee.component.html',
  styles: [],
})
export class AddEmployeeComponent implements OnInit, OnDestroy {
  // Variables
  today = new Date().getTime();
  areas: Area[];
  areaSubscription: Subscription = new Subscription();
  uiSubscription: Subscription = new Subscription();
  loading: boolean = false;

  // Estructura formulario reactivo
  createFormGroup() {
    const regex1 = new RegExp(/^[A-Z]+$/i);
    const regex2 = new RegExp(/^[A-Z0-9-]+$/i);
    const regex3 = new RegExp(/^[A-Z\s]+$/i);
    return new FormGroup({
      nameOne: new FormControl('', [
        Validators.required,
        Validators.maxLength(20),
        Validators.pattern(regex1),
      ]),
      otherNames: new FormControl(null, [
        Validators.maxLength(50),
        Validators.pattern(regex1),
      ]),
      surnameOne: new FormControl('', [
        Validators.required,
        Validators.maxLength(20),
        Validators.pattern(regex3),
      ]),
      surnameTwo: new FormControl('', [
        Validators.required,
        Validators.maxLength(20),
        Validators.pattern(regex3),
      ]),
      numID: new FormControl('', [
        Validators.required,
        Validators.pattern(regex2),
      ]),
      country: new FormControl('', Validators.required),
      area: new FormControl('', Validators.required),
      typeId: new FormControl('', Validators.required),
      admission: new FormControl('', Validators.required),
      state: new FormControl(true),
    });
  }

  employeeForm: FormGroup;
  totalPages: number;

  constructor(
    private store: Store<AppState>,
    private router: Router,
    private areaService: AreaService,
    private employeeService: EmployeeService
  ) {
    this.employeeForm = this.createFormGroup();
  }

  ngOnInit(): void {
    this.store
      .select('employee')
      .subscribe(({ totalPages }) => (this.totalPages = totalPages));
    this.areaSubscription = this.store
      .select('area')
      .subscribe(({ areas }) => (this.areas = areas));
    this.uiSubscription = this.store
      .select('ui')
      .subscribe(({ loading }) => (this.loading = loading));
  }

  // Cancelar subscripciones
  ngOnDestroy(): void {
    this.areaSubscription.unsubscribe();
    this.uiSubscription.unsubscribe();
  }

  // Crear nuevo Empleado
  async onSubmit() {
    // Iniciar carga
    this.store.dispatch(new StartLoadingAction());
    let newEmployee: Employee = {
      ...this.employeeForm.value,
      area: {
        id: parseInt(this.employeeForm.value.area),
      },
    };
    const res = await this.employeeService.createEmployee(newEmployee);
    if (res.ok) {
      newEmployee = res.body;
      this.router.navigate([`/${this.totalPages}`]);
    } else {
      showMessage('error', res.msg);
    }
    // Finalizar carga
    this.store.dispatch(new FinishLoadingAction());
  }

  // Crear nueva Area
  async onCreateArea() {
    const result: any = await showQuestion('Nueva Área', 'Ingrese el nombre');
    if (result.value) {
      let newArea: Area = {
        name: result.value[0],
      };
      newArea = await this.areaService.createArea(newArea);
      // Añadir nueva area al estado Redux
      this.store.dispatch(new AddAreaAction(newArea));
    }
  }

  get nameOne() {
    return this.employeeForm.get('nameOne');
  }

  get otherNames() {
    return this.employeeForm.get('otherNames');
  }

  get surnameOne() {
    return this.employeeForm.get('surnameOne');
  }

  get surnameTwo() {
    return this.employeeForm.get('surnameTwo');
  }

  get admission() {
    return this.employeeForm.get('admission');
  }

  get area() {
    return this.employeeForm.get('area');
  }

  get country() {
    return this.employeeForm.get('country');
  }

  get typeId() {
    return this.employeeForm.get('typeId');
  }

  get numID() {
    return this.employeeForm.get('numID');
  }
}
