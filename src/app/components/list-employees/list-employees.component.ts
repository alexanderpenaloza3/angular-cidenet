import { Component, OnDestroy, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import Employee from '../../interfaces/Employee';
import { AppState } from '../../app.reducers';
import { EmployeeService } from '../../services/employee.service';
import { showConfirmation } from '../../helpers/alerts';
import {
  DeleteEmployeeAction,
  LoadEmployeesAction,
  SetEmployeeAction,
  SetTotalEmployeesAction,
  SetTotalPagesAction,
} from '../../reducers/employee/employee.actions';

@Component({
  selector: 'app-list-employees',
  templateUrl: './list-employees.component.html',
  styles: [],
})
export class ListEmployeesComponent implements OnInit, OnDestroy {
  // Variables
  employees: Employee[];
  employeeSubscription: Subscription = new Subscription();
  filter: string = '';
  filterCountry: string = '';
  filterType: string = '';

  constructor(
    private route: ActivatedRoute,
    private store: Store<AppState>,
    private employeeService: EmployeeService
  ) {
    this.employeeSubscription = this.store
      .select('employee')
      .subscribe(({ employees }) => {
        this.employees = employees;
      });
  }

  ngOnInit(): void {
    this.route.params.subscribe((params) => {
      const { id } = params;
      this.loadEmployees(id);
    });
  }

  // Cancelar subscripciones
  ngOnDestroy(): void {
    this.employeeSubscription.unsubscribe();
  }

  // Cargar empleados, total de paginas y total de empleados de la BD
  async loadEmployees(id: number) {
    const data = await this.employeeService.loadEmployees(id);
    this.store.dispatch(new SetTotalPagesAction(data.totalPages));
    this.store.dispatch(new SetTotalEmployeesAction(data.totalEmployees));
    if (data.employees) {
      this.store.dispatch(new LoadEmployeesAction(data.employees));
    }
  }

  // Eliminar empleado
  async deleteEmployee(id: number) {
    const result = await showConfirmation(
      'Está seguro de que desea eliminar el empleado',
      ''
    );
    if (result.isConfirmed) {
      await this.employeeService.deleteEmployee(id);
      // Quitar empleado del estado Redux
      this.store.dispatch(new DeleteEmployeeAction(id));
    }
  }

  activeEmployee(employee: Employee) {
    this.store.dispatch(new SetEmployeeAction(employee));
  }
}
