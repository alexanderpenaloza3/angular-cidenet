import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from './app.reducers';
import Area from './interfaces/Area';
import { LoadAreasAction } from './reducers/area/area.actions';
import { AreaService } from './services/area.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'frontend-cidenet-employees';

  constructor(
    private store: Store<AppState>,
    private areaService: AreaService
  ) {
    this.load();
  }

  async load() {
    const areas: Area[] = await this.areaService.loadAreas();
    this.store.dispatch(new LoadAreasAction(areas));
  }
}
