import { ActionReducerMap } from '@ngrx/store';
import * as FromArea from './reducers/area/area.reducer';
import * as FromEmployee from './reducers/employee/employee.reducer';
import * as FromUI from './reducers/ui/ui.reducer';

export interface AppState {
  area: FromArea.AreaState;
  employee: FromEmployee.EmployeeState;
  ui: FromUI.UiState;
}

export const combineReducer: ActionReducerMap<AppState> = {
  area: FromArea.areaReducer,
  employee: FromEmployee.employeeReducer,
  ui: FromUI.uiReducer,
};
