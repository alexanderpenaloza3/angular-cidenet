export const environment = {
  production: true,
  urlBackend: 'https://spring-boot-cidenet.herokuapp.com',
};
